# README #

O problema é realmente um grande desafio, porém bastante dedicação e alguns dias de trabalho tornou possível um boa solução. O mesmo foi solucionado utilizando a linguagem de programação Python e Framework Django que tornaram o desenvolvimento bem ágil.

Uma grande motivação?? Estar trabalhando com Python e Django. 

**=)**

### Copia do Repositório ###

    git clone https://celioantony@bitbucket.org/celioantony/walmart.git

### Requisitos ###
Sistema operacional Linux / Windows 7

** Pacotes **

* Python (2.7.x)
* Pip (7.1.2) https://pip.pypa.io/en/stable/installing/
* Virtualenv  (com o Pip instalado executar o comando)
       - pip install virtualenv


### Criando ambiente do sistema ###
Para criação do ambiente execute os seguinte comandos:

* $ virtualenv venvwalmart (Criar ambiente virtual)
* $ source venvwalmart/bin/activate (Ativar ambiente virtual)
* $ pip install -r requirements.txt (Instala todas as dependências)
* $ cd walmart/walmart (Acessar repo. clonado)
* $ python manage.py makemigrations base (Cria base de dados para aplicação base)
* $ python manage.py migrate
* $ python manage.py createsuperuser (Cria usuário admin 'Opcional')
     - User: admin, Password: admin

### Executar o sistema ###
Com o ambiente virtual ativado, executar o seguinte comando:

* $ python manage.py runserver

OBS: Com isso o sistema pode ser acessado pelo navegador com o endereço.

    http://localhost:8000