function clear_fields(){
    document.getElementById('information-route').disabled = false;
    document.getElementById('information-route').value = '';
    document.getElementById('description-route').value = '';
    // document.getElementById('file-information-route').value = '';
}

function send_informations_route(){

    var description = $('#description-route').val();
    var information = $('#information-route').val();

    // information = jQuery.grep(information, function(value){
    //   return value != "";
    // })

    data = {
        'description': description,
        'information': information,
    };

    $.ajax({
        url: 'receivelogisticsmesh',
        method: 'POST',
        // data: $('#form-information-route').serialize(),
        data: data,
        success: function (response) {
                clear_fields();
                update_list_map(response);
                autocomplete_maps();
            }
    })
    .done(show_messages)
    ;
}

function update_list_map(response){

    var element_li = document.createElement('li');
    var element_a = document.createElement('a');
    var element_ul = document.getElementById('maps_list');

    if(response.new_mesh){
      element_a.innerHTML = response.new_mesh['description'];
      element_li.setAttribute('class', 'list-group-item');
      element_li.setAttribute('id', 'li_'+response.new_mesh['id']);
      element_a.setAttribute('code', response.new_mesh['id']);
      element_a.setAttribute('href','#');
      element_a.setAttribute('data-toggle', 'modal');
      element_a.setAttribute('data-target', '#modal-graph');
      element_a.setAttribute('onclick', 'show_graph('+response.new_mesh['id']+')');
      element_li.appendChild(element_a);
      element_ul.appendChild(element_li);
    }
}

function upload_file(event) {

  event.preventDefault();
  var data = new FormData();

  var file = document.getElementById('file-informations-route').files[0];
  var information_route = document.getElementById('information-route');
  data.append('file', file);

  $.ajax({
      url: 'uploadfile',
      type: 'POST',
      data: data,
      cache: false,
      processData: false,
      contentType: false,
      success: function(data) {
          information_route.value = '';
          for(index in data.data){
             information_route.value += data.data[index]+'\n'
          }
          information_route.disabled = true;
          autocomplete_maps();
      }
  })
  .done(function(response){
      show_messages(response);
  })
  ;
  return false;

}

function get_local(){

  var origin = [];
  var target = [];
  map_description = $('#maps').val();

  $.ajax({
        url: 'gettarget',
        method: 'GET',
        data: {'map_description': map_description},
        success: function(data) {
              for(index in data.target){
                origin.push(data.target[index]);
                target.push(data.target[index]);
              }
        } 
    })
    .done(show_messages);
    ;

  $('#origin').autocomplete({
    source: origin
  });
  $('#target').autocomplete({
    source: target
  })
}

function search_path(){
    form_searchpath = $('#form-searchpath').serialize();

    el_map = document.getElementById('result_map');
    el_shortestpath = document.getElementById('result_shortestpath');
    el_distance = document.getElementById('result_distance');
    el_costliter = document.getElementById('result_costliter');

    $.ajax({
        url: 'searchpath',
        method: 'POST',
        data: form_searchpath,
        success: function(data) {
          el_map.innerHTML = '';
          if(data.messages == null){

              for(index in data.graph){
                el_map.innerHTML += data.graph[index].join(' ')+'<br>';
              }
              el_shortestpath.innerHTML = data.shortest_path;
              el_distance.innerHTML = data.distance + ' Km';
              el_costliter.innerHTML = 'R$ '+ data.cost_path;

              simulateClick()
          }

        } 
    })
    .done(show_messages)
    ;
}

function autocomplete_maps(){
    var maps = [];
    var data;
    $.ajax({
        url: 'maps',
        method: 'GET',
        success: function(data) {
          for(index in data.maps){
              maps.push(data.maps[index].description);
          } 
        } 
    })
    .done(show_messages);
    ;

    $('#maps').autocomplete({
        source: maps
      });

}

function simulateClick() {
  var evt = document.createEvent("MouseEvents");
  evt.initMouseEvent("click", true, true, window,
    0, 0, 0, 0, 0, false, false, false, false, 0, null);
  var cb = document.getElementById("result"); 
  var canceled = !cb.dispatchEvent(evt);
}

function show_graph(code){

  el_mesh_content = document.getElementById('mesh-content');
  el_mesh_content.innerHTML = '';

      $.ajax({
        url: 'maps',
        method: 'POST',
        data: {'map_id': code},
        success: function(data) {

          el_mesh_content.innerHTML = '';
          for(index in data.graph){
                el_mesh_content.innerHTML += data.graph[index].join(' ')+'<br>';
          }
        } 
    })
    .done(show_messages);
}

$(function(){
  autocomplete_maps();

  // show_graph();
});