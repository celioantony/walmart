from django.conf.urls import include, url
from django.contrib import admin
from rest_framework import routers
from base.views import management

router = routers.DefaultRouter()
router.register(r'logisticsmesh', management.LogisticsMeshViewSet)
router.register(r'mesh', management.MeshViewSet)

urlpatterns = [
    # url(r'^admin/', include(admin.site.urls)),
    url(r'', include('base.urls')),
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
