# -*- encoding: utf8 -*-

import json
from django.shortcuts import render
from django.db import transaction
from django.http import JsonResponse
from django.contrib import messages
from django.core.paginator import Paginator
from itertools import chain
from rest_framework import viewsets
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer

from serializers import LogisticsMeshSerializer, MeshSerializer
from base.models.logistics_mesh import LogisticsMesh, Mesh
from base.forms.forms import LogisticsMeshForm, SearchPathForm
from base.views.graph import Graph
from django.conf import settings
from base.essential import render_messages



class LogisticsMeshViewSet(viewsets.ModelViewSet):
    """
    API - permite visualizar e editar malhas logisticas
    """

    queryset = LogisticsMesh.objects.all()
    serializer_class = LogisticsMeshSerializer

class MeshViewSet(viewsets.ModelViewSet):
    """
    API - permite visualizar e editar malhas logisticas
    """

    queryset = Mesh.objects.all().order_by('description')
    serializer_class = MeshSerializer


def index(request, template_name='index.html'):
    """
    Return o página principal da aplicação e seus parametros
    """

    logistics_mesh_form = LogisticsMeshForm()
    search_path_form = SearchPathForm()

    return render(request, template_name,{
        'logistics_mesh_form': logistics_mesh_form,
        'search_path_form': search_path_form,
        'maps_list': Mesh.objects.all(),
        })


def logistics_mesh_view(request):
    """
    Função criada para persistir novos mapas no banco de dados.
    """
    
    if request.method == 'POST':

        description = str(request.POST.get('description')) or None
        list_mesh = request.POST.get('information').replace('\r','').split('\n') or None
        list_mesh = [tuple(m.split(' ')) for m in list_mesh if len(m)!=0]
        mesh = None
        created = None

        data = dict()

        if description is not None and len(list_mesh)!=0:

            with transaction.atomic():
                try:
                    mesh, created = Mesh.objects.get_or_create(description=description)

                    if created:
                        for origin, target, distance in list_mesh:
                            logistics_mesh = LogisticsMesh.objects.create(
                                mesh=mesh, origin=origin, target=target, distance=distance
                                )

                except Exception as e:
                    created = None
                    message='Error no formato das informações enviadas.'
                    messages.add_message(request, messages.ERROR, message)

                if created:
                    mesh_serializer = MeshSerializer(mesh)
                    message='Mapa Criado.'
                    messages.add_message(request, messages.SUCCESS, message)
                    data = {'new_mesh': mesh_serializer.data, 
                            'messages': render_messages(request), 
                            'description': description}

                    return JsonResponse(data)

                elif created == False:
                    message = 'Mapa já existente.'
                    messages.add_message(request, messages.ERROR, message)
                    data = {'new_mesh': None, 'messages': render_messages(request)}
                    return JsonResponse(dict(messages=render_messages(request)))

        else:
            message = 'Descrição do mapa e/ou malha não foram preenchidos'
            messages.add_message(request, messages.INFO, message)

        data = {'new_mesh': None, 'messages': render_messages(request)}
        return JsonResponse(data)


def upload_file(request):
    """
    Upload de arquivo de texto para malha logística.
    """

    uploaded_filename = request.FILES['file'].name
    uploaded_file = request.FILES['file']

    mesh_list = None
    for chunk in uploaded_file.chunks():
        mesh_list = str(chunk).split('\n')

    return JsonResponse(dict(data=mesh_list))


def get_map_names(request):
    """
    Returna o nome dos mapas (GET) ou a malha logística (POST)
    """

    if request.method == 'GET':
        maps = list(Mesh.objects.all().values('id','description'))

        return JsonResponse(dict(maps=maps))

    if request.method == 'POST':
        map_id = request.POST.get('map_id')
        mesh = Mesh.objects.get(id=map_id)
        parameters = list(LogisticsMesh.objects.filter(mesh=mesh).values_list('origin','target','distance'))

        return JsonResponse(dict(graph=parameters))

def get_target(request):
    """
    Returna todos os vértices do mapa.
    """
    if request.method == 'GET':
        mesh_description = request.GET.get('map_description')
        target = None

        try:
            mesh = Mesh.objects.get(description=mesh_description)
            target = sorted(
                list(set(chain(*LogisticsMesh.objects.filter(
                    mesh=mesh.id).values_list('origin','target'))))
                )

        except Exception as e:
            message = 'Não existem mapas cadastrados.'
            messages.add_message(request, messages.ERROR, message)

        if target:
            return JsonResponse(dict(target=target))

    data = {'messages': render_messages(request)}
    return JsonResponse(data)


def search_path(request):
    """
    Returna o melhor trajeto, distância e custo.
    """

    if request.method == 'POST':

        form = SearchPathForm(request.POST)

        if form.is_valid():

            graph = Graph()
            try:

                map = form.cleaned_data['maps']
                origin = form.cleaned_data['origin']
                target = form.cleaned_data['target']
                autonomy = float(form.cleaned_data['autonomy'])
                cost_liter = float(form.cleaned_data['cost_liter'].replace(',','.'))

                mesh = Mesh.objects.get(description=map)
                parameters = LogisticsMesh.objects.filter(mesh=mesh).values_list('origin','target','distance')

                graph.mount_graph(parameters)
                shortest, distance = graph.shortest_path_and_distance(origin, target)
                cost_path = graph.calc_cost_path(autonomy, cost_liter, distance)

            except Exception as e:
                print '>>>> ', e

            data = dict(
                graph=list(parameters),
                shortest_path=shortest, 
                distance=distance, 
                cost_path=cost_path, 
                messages=None)

            return JsonResponse(data)
        else:
             messages.add_message(request, messages.ERROR, form.msg_error)
             return JsonResponse(dict(messages=render_messages(request)))

        message='Error inesperado.'
        messages.add_message(request, messages.ERROR, message)
        return JsonResponse(dict(messages=render_messages(request)))

