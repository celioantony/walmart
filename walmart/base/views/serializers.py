from rest_framework import serializers

from base.models.logistics_mesh import LogisticsMesh, Mesh


class LogisticsMeshSerializer(serializers.ModelSerializer):

    """
    Serializer data
    """
    class Meta:
        model = LogisticsMesh
        fields = ('mesh', 'origin', 'target', 'distance')


class MeshSerializer(serializers.ModelSerializer):

    """
    Serializer data
    """
    class Meta:
        model = Mesh
        fields = ('id', 'description',)


