import networkx as nx

class Graph():

    def __init__(self):
        self.graph = nx.DiGraph()

    def mount_graph(self, weight_edges):
        """
        Mount the graph with respective parameters weight_edges=[(source, target, weight),...]
        """
        self.graph.add_weighted_edges_from(weight_edges)

    def shortest_path_and_distance(self, origin, target):
        """
        Method return shortest path and distance entries the points
        """

        return (nx.dijkstra_path(self.graph, origin, target), 
                nx.dijkstra_path_length(self.graph, origin, target))

    def calc_cost_path(self, autonomy, value_liter, distance):
        """
        Method return calc of cost of path
        """
        return (value_liter*distance)/autonomy