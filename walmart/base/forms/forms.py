# -*- encoding: utf8 -*-

from django import forms
# from uni_form.helper import FormHelper
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Div, Submit, HTML, Hidden
from crispy_forms.bootstrap import FormActions, StrictButton, InlineCheckboxes

from base.models.logistics_mesh import LogisticsMesh


class LogisticsMeshForm(forms.Form):
    """
    Form para permitir novos registros de mapas.
    """

    description = forms.CharField(
        label='Descrição',
        max_length=60
        )
    information_route = forms.CharField(
        label='Informações',
        help_text='Informe os dados no formato apropriado.',
        widget=forms.Textarea
        )
    file_informations_route = forms.FileField(
        label='Arquivo',
        )

    def __init__(self, *args, **kwargs):
        super(LogisticsMeshForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        # self.helper.form_method = ''
        self.helper.form_id = 'form-information-route'
        self.helper.form_class = 'form-horizontal'


        self.helper.layout = Layout(
            Field('description',
                  id='description-route',
                  css_class='form-control',
                  placeholder='Nome do mapa',
                  ),
            Field('information_route',
                  id='information-route',
                  css_class='form-control',
                  placeholder='Malha logística',
                  ),
            Field('file_informations_route',
                id='file-informations-route',
                css_class='btn btn-default btn-file',
                onchange='upload_file(event)',
                # placeholder='...',
                ),
            HTML("""
                <div class="" role="group">
                  <button type="reset" id="reset" type="reset" class="btn btn-warning" onclick="clear_fields()">
                  Limpar Campos
                  </button>
                  <button type="button" class="btn btn-default btn-success" onclick="send_informations_route()">
                  Enviar
                  </button>
                </div>
                """
                ),
            # StrictButton('Limpar Campos',
            #          id='reset' ,
            #          type='reset',
            #          css_class='btn btn-default btn-warning',
            #          onclick='clear_fields()',
            #             ),
            # StrictButton('Enviar',
            #          # type="",
            #          css_class="btn btn-success",
            #          onclick="send_informations_route()",
            #             ),
        )


class SearchPathForm(forms.Form):
    """
    Form para permitir buscar de caminhos e custos.
    """

    maps = forms.CharField(label='Mapa', required=False)
    origin = forms.CharField(label='Origem', required=False)
    target = forms.CharField(label='Destino', required=False)
    autonomy = forms.CharField(label='Autonômia', required=False)
    cost_liter = forms.CharField(label='Custo litro', required=False)

    msg_error = None

    def __init__(self, *args, **kwargs):
        super(SearchPathForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        # self.helper.form_method = ''
        self.helper.form_id = 'form-searchpath'
        self.helper.form_class = 'form-horizontal'


        self.helper.layout = Layout(
            Field('maps',
                  id='maps',
                  css_class='form-control ui-widget',
                  placeholder='Informe o nome mapa.',
                  onchange='get_local()'
                  ),
            Field('origin',
                  id='origin',
                  css_class='form-control',
                  placeholder='Informe a origem da viagem.'
                  ),
            Field('target',
                  id='target',
                  css_class='form-control',
                  placeholder='Informa o destino da viagem.'
                  ),
            Field('autonomy',
                  id='autonomy',
                  css_class='form-control',
                  placeholder='Informe a autonômia do veículo.'
                  ),
            Field('cost_liter',
                  id='cost_liter',
                  css_class='form-control',
                  placeholder='Informe o custo por litro de combustivel.'
                  ),
            HTML("""
                <br>
                <div class="" role="group">
                  <button type="reset" id="reset" type="reset" class="btn btn-warning">
                  Limpar Campos
                  </button>
                  <button type="button" class="btn btn-default btn-success" onclick="search_path()">
                  Enviar
                  </button>
                </div>
                """
                ),
            # StrictButton('Limpar Campos',
            #          id='reset' ,
            #          type='reset',
            #          css_class='btn btn-default btn-warning',
            #             ),
            # StrictButton('Enviar',
            #          # type="",
            #          css_class="btn btn-default btn-success",
            #          onclick='search_path()',
            #             ),
        )

    
    def clean(self):
        """
        Valida campos obrigatórios no Form.
        """
        cleaned_data = super(SearchPathForm, self).clean()

        if cleaned_data['maps'] == "":
            self.msg_error = "Campo Mapa é obrigatório"
            raise forms.ValidationError("Campo Mapa é obrigatório.")
        if cleaned_data['origin'] == "":
            print '>>>>>> aquiiiiiii'
            self.msg_error = "Campo Origem é obrigatório."
            raise forms.ValidationError("Campo Origem é obrigatório.")
        if cleaned_data['target'] == "":
            self.msg_error = "Campo Destino é obrigatório."
            raise forms.ValidationError("Campo Destino é obrigatório.")
        if cleaned_data['autonomy'] == "":
            self.msg_error = "Campo Autonômia é obrigatório."
            raise forms.ValidationError("Campo Autonômia é obrigatório.")
        if cleaned_data['cost_liter'] == "":
            self.msg_error = "Campo Custo Litro é obrigatório."
            raise forms.ValidationError("Campo Custo Litro é obrigatório.")

        return cleaned_data