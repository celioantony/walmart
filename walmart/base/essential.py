from django.shortcuts import render

def render_messages(request):
    return render(request, '_messages.html').content.strip()