from django.conf.urls import include, url

from base.views import management


urlpatterns = [
    url(r'^$', management.index, name='index'),
    url(r'^receivemesh$', management.Mesh, name='mesh'),
    url(r'^receivelogisticsmesh$', management.logistics_mesh_view, name='logisticsmesh'),
    url(r'^uploadfile$', management.upload_file, name='upload_file'),
    url(r'^maps', management.get_map_names, name='maps_names'),
    url(r'^gettarget', management.get_target, name='get_target'),
    url(r'^searchpath', management.search_path, name='search_path'),
]