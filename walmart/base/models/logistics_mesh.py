# -*- encoding: utf8 -*-
from django.db import models


class Mesh(models.Model):
    """
    Modelo que define a descrição do mapa.
    """

    class Meta:
        app_label = 'base'

    description = models.CharField(max_length=60, null=False)


class LogisticsMesh(models.Model):
    """
    Modelo que define malha logística do mapa.
    """

    class Meta:
        app_label = 'base'

    mesh = models.ForeignKey(Mesh)
    origin = models.CharField(max_length=60, null=False)
    target = models.CharField(max_length=60, null=False)
    distance = models.FloatField(null=False)
